package game.webserver.controller

import game.webserver.DataBase
import io.vertx.core.http.Cookie
import io.vertx.ext.web.RoutingContext

class WebController {
  fun authPage(routingContext: RoutingContext) {
    routingContext.response().setStatusCode(200)
      .putHeader("content-type","text/html")
      .sendFile("index.html")
  }
  fun loginPage(routingContext: RoutingContext) {
    routingContext.response().setStatusCode(200).putHeader("content-type","text/html")
      .end(DynamicPage().html("login"))
  }
  fun registerPage(routingContext: RoutingContext) {
    routingContext.response().setStatusCode(200).putHeader("content-type","text/html")
      .end(DynamicPage().html("register"))
  }
  fun playPage(routingContext: RoutingContext) {
    val id = routingContext.request().getCookie("id").value
    routingContext.response().setStatusCode(200).putHeader("content-type","text/html")
      .end(DynamicPage().html("play","Your game id = $id",1))
  }
  fun waitingPage(routingContext: RoutingContext, userId:String, gameId:String, connectionType:String) {
    val name = routingContext.request().getCookie("name").value
    routingContext.response().setStatusCode(200).putHeader("content-type","text/html")
      .setChunked(true)
      .addCookie(Cookie.cookie("connectionType", connectionType))
      .addCookie(Cookie.cookie("gameId", gameId))
      .end(DynamicPage().html("waiting"))
  }
  fun gamePage(routingContext: RoutingContext) {
    val name = routingContext.request().getCookie("name").value
    val id = routingContext.request().getCookie("id").value
    val connType = routingContext.request().getCookie("connectionType").value
    val uniqueCombinedId = "$name#$id#$connType"
    val paramName = routingContext.request().getParam("user")
    val paramId = routingContext.request().getParam("userId")
    val paramConnType = routingContext.request().getParam("connectionType")
    val paramUniqueId = "$paramName#$paramId#$paramConnType"
    if(uniqueCombinedId==paramUniqueId) {
      val gameId = routingContext.request().getParam("gameId")
      routingContext.response().setStatusCode(200).putHeader("content-type", "text/html")
        .end(DynamicPage().play(uniqueCombinedId, gameId))
    }
    else WebController().gameNotFound(routingContext,"notFounded")
  }
  fun access(routingContext: RoutingContext,id:String,name:String) {
    val page = DynamicPage().html("access","Hello $name#$id",1)
    routingContext.response().setStatusCode(200)
      .putHeader("content-type", "text/html")
      .setChunked(true)
      .addCookie(Cookie.cookie("name",name))
      .addCookie(Cookie.cookie("id",id))
      .end(page)
  }
  fun denied(routingContext: RoutingContext,key:String) {
    routingContext.response().setStatusCode(200)
      .putHeader("content-type","text/html").end(DynamicPage().html(key))
  }
  fun gameNotFound(routingContext: RoutingContext,key:String) {
    val id = routingContext.request().getCookie("id").value
    routingContext.response().setStatusCode(200)
      .putHeader("content-type","text/html")
      .end(DynamicPage().html(key,"Your Room id = $id<br><hPut>Game not found</hPut>",1))
  }
  fun loginHandler(routingContext:RoutingContext) {
    val request = routingContext.request()
    val name = request.getParam("user")
    val password = request.getParam("password")
    val database = DataBase()
    database.login(routingContext,database.openDataBase(),name,password)
  }
  fun registerHandler(routingContext: RoutingContext) {
    val request = routingContext.request()
    val name:String = request.getParam("user")
    val password:String = request.getParam("password")
    val database = DataBase()
    database.nameCheck(routingContext,database.openDataBase(),name,password)
  }
  fun createHandler(routingContext: RoutingContext) {
    val hostId = routingContext.request().getCookie("id").value
    val database = DataBase()
    database.createRoom(routingContext,database.openDataBase(),hostId)
  }
  fun joinHandler(routingContext: RoutingContext) {
    val playerId = routingContext.request().getCookie("id").value
    val gameId:String = routingContext.request().getParam("gameId")
    val database = DataBase()
    database.joinRoom(routingContext, database.openDataBase(), playerId, gameId)
  }
  fun gameHandler(routingContext: RoutingContext, userId:String, gameId:String, connectionType:String) {
    val name = routingContext.request().getCookie("name").value
    routingContext.response().setStatusCode(200).putHeader("content-type","text/html")
      .setChunked(true)
      .addCookie(Cookie.cookie("connectionType", connectionType))
      .addCookie(Cookie.cookie("gameId", gameId))
      .end("<script> document.location.href = '/gamePage?user=$name&userId=$userId&connectionType=$connectionType&gameId=$gameId' </script>")
  }
  fun finishHandler(routingContext: RoutingContext) {
    val page = DynamicPage().html("access","Play again?",1)
    routingContext.response().setStatusCode(200).putHeader("content-type","text/html")
      .end(page)
  }

}

