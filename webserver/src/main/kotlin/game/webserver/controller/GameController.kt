package game.webserver.controller

import game.webserver.DataBase
import io.vertx.ext.web.RoutingContext
import io.vertx.mysqlclient.MySQLPool


class GameController {
  fun update(routingContext: RoutingContext) {
    val database = DataBase()
    val gameId:String = routingContext.request().getCookie("gameId").value
    val connType:String = routingContext.request().getCookie("connectionType").value
    val initL: String = routingContext.request().getParam("initL")
    val initT: String = routingContext.request().getParam("initT")
    val lionH: String = routingContext.request().getParam("lionH")
    val tigerH: String = routingContext.request().getParam("tigerH")
    database.checkUpdates(routingContext, database.openDataBase(), initL, initT, lionH, tigerH, gameId, connType)
  }

  fun serverUpdateDrawer(routingContext: RoutingContext, client:MySQLPool, gameId:String, connType:String, initL: String, initT: String, serverInitL:String, serverInitT:String,
      lionH:String, tigerH:String, serverLionH:String, serverTigerH:String, animation:Int) {
    println(animation)
    if((initL==serverInitL) && (initT==serverInitT) && (lionH==serverLionH) && (tigerH==serverTigerH) && (animation == 0)) {
      client.close()
      routingContext.response().setStatusCode(200).putHeader("content-type", "text/html")
        .end("Server is working")
    } else {
      when(animation) {
        -6, -5, -2, -1 -> {
          AnimationController().synchronizeAfterMove(routingContext, initL, initT, serverInitL, serverInitT, animation)
          DataBase().updateAnimation(client, connType, gameId,0)
        }
        0 -> {
          client.close()
          AnimationController().synchronize(routingContext, initL, initT, serverInitL, serverInitT, serverLionH, serverTigerH)
        }
        1 -> {
          AnimationController().lionMoves(routingContext, initL, serverInitL)
          DataBase().updateAnimation(client, connType, gameId, -1)
        }
        2 -> {
          AnimationController().lionAttack(routingContext, serverInitL, serverInitT)
          DataBase().updateAnimation(client, connType, gameId,-2)
        }
        5 -> {
          AnimationController().tigerMoves(routingContext, initT, serverInitT)
          DataBase().updateAnimation(client, connType, gameId,-5)
        }
        6 -> {
          AnimationController().tigerAttack(routingContext, serverInitL, serverInitT)
          DataBase().updateAnimation(client, connType, gameId,-6)
        }
      }
    }
  }

  fun moveHandler(routingContext: RoutingContext) {
    val request = routingContext.request()
    val userId = request.getCookie("id").value
    val connType = request.getCookie("connectionType").value
    val gameId = request.getCookie("gameId").value
    val move = request.getParam("move").toInt()
    val database = DataBase()
    database.checkTurn(routingContext, database.openDataBase(), connType, gameId, userId, move)
  }

  fun logicToMove(routingContext:RoutingContext, client:MySQLPool, connType:String, gameId:String, newLockId:String,
                  initL:Int, initT:Int, move:Int, healthL:Int, healthT:Int) {
    val database = DataBase()
    var sub:Int
    if (connType == "host") sub = move - initL
    else sub = move - initT
    val shift = Math.floorMod(sub, 9)
    val rows = (Math.abs(sub) + 1) / 9
    if (((shift in 0..1) || shift == 8) && (rows in 0..1)) {
      println("Moves to $move")
      if (connType == "host") {
        when (move) {
          initT -> {
            val health = healthT - 25
            if(health <= 0) database.updateLockerMove(client, connType, gameId, newLockId, move, health, 1)
              else database.updateLockerAttack(client, connType, gameId, newLockId, health, 2)
          }
          22 -> {
            val health = healthL + 25
            database.updateLockerTotem(client, connType, gameId, newLockId, health)
          }
          else -> {
            val health = healthL + 5
            database.updateLockerMove(client, connType, gameId, newLockId, move, health, 1)
          }
        }
      } else {
        when (move) {
          initL -> {
            val health = healthL - 25
            if(health <= 0) database.updateLockerMove(client, connType, gameId, newLockId, move, health, 5)
              else database.updateLockerAttack(client, connType, gameId, newLockId, health, 6)
          }
          22 -> {
            val health = healthT + 25
            database.updateLockerTotem(client, connType, gameId, newLockId, health)
          }
          else -> {
            val health = healthT + 5
            database.updateLockerMove(client, connType, gameId, newLockId, move, health, 5)
          }
        }
      }
    } else {
      client.close()
      printFieldWrong(routingContext)
    }
  }

  fun printFieldWrong(routingContext: RoutingContext){
    routingContext.response().setStatusCode(200).putHeader("content-type", "text/html")
      .end("Not your turn")
  }

}

