package game.webserver.controller

class DynamicPage {
  fun html(key:String, text:String = "", type:Int = -1):String{
    val mainPage:String = pageMain
    val template:String = pageMap.getValue(key)
    return when(type){
      -1 -> mainPage.replace("<putbody>", template)
      else -> putText(mainPage.replace("<putbody>", template), text)
    }
  }

  private fun putText(page: String, text: String): String {
    return page.replace("<puttext>", "<hPut>$text</hPut>")
  }

  fun play(name: String, gameId: String): String {
    return pageGame.replace("<puttext>","$name, GameId = $gameId")
  }

  private val pageMain = """
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <style>
    body{
      background: url(assets/image/mainbackground.jpg) no-repeat;
      background-size: cover;}
  </style>
<title>Lions&Tigers</title>
</head>
<body>
  <div class="container">
    <div class="header-block">
      <div class="images">
        <div class="lion"><img src="assets/image/lion.png" class="lion"></div>
        <div class="tiger"><img src="assets/image/tiger.png" class="tiger"></div>
      </div>
      <div class="gif">
        <a href="/" style="text-decoration: none;">
        <img src="assets/image/logo.gif" class="logo">
        </a>
     </div>
     <puttext>
    </div>
   <putbody>
  </div>
</body>
</html>
</html>""".trimIndent()
  private val pageGame = """
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <style>
    body{
      background: url(assets/image/mainbackground.jpg) no-repeat;
      background-size: cover;}
  </style>
  <title>Game</title>
</head>
<body>
  <div class="wrapper" style="width:100%;height:100%">
    <div class="table-body">
      <div class="table-body-players">
        <puttext>
        <div class="table-body-players-card" id="host">
          <div class="table-body-players-card-body">
            <div class="table-body-players-card-body-nick">
              <div class="_nick">
                <div> <putname-host> </div>
              </div>
            </div>
            <div class="table-body-players-card-body-hp">
              Lion:<input type="text" id="initL" value="10">
              Health:<input type="text" id="lionH" value="100">
            </div>
          </div>
        </div>
        <div class="table-body-players-card" id="player">
          <div class="table-body-players-card-body">
            <div class="table-body-players-card-body-nick">
              <div class="_nick">
                <div> <putname-player> </div>
              </div>
            </div>
            <div class="table-body-players-card-body-hp">
              Tiger:<input type="text" id="initT" value="34">
              Health:<input type="text" id="tigerH" value="100">
            </div>
          </div>
        </div>
        <iframe id="press_iframe" style="display:none" width="160" height="75" frameborder="1"> </iframe>
        <iframe id="update_iframe" style="display:none" width="160" height="75" frameborder="1"> </iframe>
      </div>
      <div class="table-body-board">
        <table class="game-board">
          <tbody>
            <tr class="game-row">
              <td> <img class="click" id="0" src="/assets/fields/forest.png">
              <td> <img class="click" id="1" src="/assets/fields/forest.png">
              <td> <img class="click" id="2" src="/assets/fields/forest.png">
              <td> <img class="click" id="3" src="/assets/fields/forest.png">
              <td> <img class="click" id="4" src="/assets/fields/forest.png">
              <td> <img class="click" id="5" src="/assets/fields/forest.png">
              <td> <img class="click" id="6" src="/assets/fields/forest.png">
              <td> <img class="click" id="7" src="/assets/fields/forest.png">
              <td> <img class="click" id="8" src="/assets/fields/forest.png">
            </tr>
            <tr class="game-row">
              <td> <img class="click" id="9" src="/assets/fields/forest.png">
              <td> <img class="click" id="10" src="/assets/fields/forest_lion.png">
              <td> <img class="click" id="11" src="/assets/fields/forest.png">
              <td> <img class="click" id="12" src="/assets/fields/forest.png">
              <td> <img class="click" id="13" src="/assets/fields/forest.png">
              <td> <img class="click" id="14" src="/assets/fields/forest.png">
              <td> <img class="click" id="15" src="/assets/fields/forest.png">
              <td> <img class="click" id="16" src="/assets/fields/forest.png">
              <td> <img class="click" id="17" src="/assets/fields/forest.png">
            </tr>
            <tr class="game-row">
              <td> <img class="click" id="18" src="/assets/fields/forest.png">
              <td> <img class="click" id="19" src="/assets/fields/forest.png">
              <td> <img class="click" id="20" src="/assets/fields/forest.png">
              <td> <img class="click" id="21" src="/assets/fields/forest.png">
              <td> <img class="click" id="22" src="/assets/fields/totem.png">
              <td> <img class="click" id="23" src="/assets/fields/forest.png">
              <td> <img class="click" id="24" src="/assets/fields/forest.png">
              <td> <img class="click" id="25" src="/assets/fields/forest.png">
              <td> <img class="click" id="26" src="/assets/fields/forest.png">
            </tr>
            <tr class="game-row">
              <td> <img class="click" id="27" src="/assets/fields/forest.png">
              <td> <img class="click" id="28" src="/assets/fields/forest.png">
              <td> <img class="click" id="29" src="/assets/fields/forest.png">
              <td> <img class="click" id="30" src="/assets/fields/forest.png">
              <td> <img class="click" id="31" src="/assets/fields/forest.png">
              <td> <img class="click" id="32" src="/assets/fields/forest.png">
              <td> <img class="click" id="33" src="/assets/fields/forest.png">
              <td> <img class="click" id="34" src="/assets/fields/forest_tiger.png">
              <td> <img class="click" id="35" src="/assets/fields/forest.png">
            </tr>
            <tr class="game-row">
              <td> <img class="click" id="36" src="/assets/fields/forest.png">
              <td> <img class="click" id="37" src="/assets/fields/forest.png">
              <td> <img class="click" id="38" src="/assets/fields/forest.png">
              <td> <img class="click" id="39" src="/assets/fields/forest.png">
              <td> <img class="click" id="40" src="/assets/fields/forest.png">
              <td> <img class="click" id="41" src="/assets/fields/forest.png">
              <td> <img class="click" id="42" src="/assets/fields/forest.png">
              <td> <img class="click" id="43" src="/assets/fields/forest.png">
              <td> <img class="click" id="44" src="/assets/fields/forest.png">
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <script src="/js/main.js"></script>
  </body>
</html>
""".trimIndent()

  private val waitingPage = Pair("waiting","""
    <div class="create-join">
      <hPut>Waiting a Players</hPut>
      <iframe src="/waiting" id="waiting_iframe" style="display:" width="240" height="100" frameborder="1"> </iframe>
    </div>
    <script>

    </script>
    """.trimIndent())

  private val pageAuthorization = Pair("auth","""
   <div class="login-register">
      <button class="login" onclick="document.location = '/enter';"> Login </button>
      <button class="register" onclick="document.location = '/registration';"> Register </button>
   </div>""".trimIndent())

  private val pageLogin = Pair("login","""
    <form class="user-info" method="post" action="/login">
      <input type="text" name="user" placeholder="Nickname" required>
      <input type="password" name="password" placeholder="Password" required>
      <button type="submit" class="btn-submit"> Submit </button>
    </form>""".trimIndent())

  private val pageRegister = Pair("register","""
    <form class="user-info" method="post" action="/register">
      <input type="text" name="user" placeholder="Nickname" required>
      <input type="password" name="password" placeholder="Password" required>
      <button type="submit" class="btn-submit"> Submit </button>
    </form>""".trimIndent())

  private val pageMenu = Pair("access","""
     <div class="form-control">
          <button class="play" onclick="document.location = '/play';"> Play </button>
          <button class="stats">Stats</button>
          <button class="info">Info</button>
      </div>""".trimIndent())

  private val pagePlay = Pair("play","""
    <div class="create-join">
        <button class="create" onclick="document.location= '/create';"> Create </button>
        <form class="form-control" method="get" action="/join">
          <button type="submit"> Join </button>
          <input class="inputId" type="text" name="gameId" placeholder="Game id"required>
        </form>
    </div>""".trimIndent())

  /* ---------- Error Pages ---------- */

  private val pageDenied = Pair("denied","""
    <form class="user-info" method="post" action="/login">
      <hPut>Incorrect Password</hPut>
      <input type="text" name="user" placeholder="Nickname" required>
      <input type="password" name="password" placeholder="Password" required>
      <button type="submit" class="btn-submit"> Submit </button>
    </form>""".trimIndent())

  private val pageAlready = Pair("already","""
    <form class="user-info" method="post" action="/register">
      <hPut>The name is already taken</hPut>
      <input type="text" name="user" placeholder="Nickname" required>
      <input type="password" name="password" placeholder="Password" required>
      <button type="submit" class="btn-submit"> Submit </button>
    </form>""".trimIndent())

  private val pageNotFounded = Pair("notFounded","""
    <div class="create-join">
        <button class="create" onclick="document.location= '/create';"> Create </button>
        <form class="form-control" method="get" action="/join">
          <button type="submit"> Join </button>
          <input class="inputId" type="text" name="gameId" placeholder="Game id"required>
        </form>
   </div>""".trimIndent())

  private val pageMap = mapOf(pageAuthorization,pageLogin,pageRegister,pageDenied,pageAlready,pageMenu,pagePlay,waitingPage,pageNotFounded)
}

