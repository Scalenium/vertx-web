package game.webserver.controller
import io.vertx.ext.web.RoutingContext

class DefaultController
{
  fun handleError404(routingContext: RoutingContext) {
    routingContext
      .response()
      .setStatusCode(404)
      .end("Error 404")
  }
}
