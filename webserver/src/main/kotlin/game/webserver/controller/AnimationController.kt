package game.webserver.controller
import io.vertx.ext.web.RoutingContext


class AnimationController {
  fun synchronize(routingContext: RoutingContext, initL: String, initT: String, serverInitL: String, serverInitT: String,
    serverLionH: String, serverTigerH: String) {
      routingContext.response().setStatusCode(200).putHeader("content-type", "text/html")
      .end("""
          <script>
          if($serverInitL == $serverInitT) {
            if($serverLionH <= 0) alert("Lion Wins");
            if($serverTigerH <= 0) alert("Tiger Wins");
            parent.document.location = '/finish';
          }
          parent.document.getElementById('$initL').src='/assets/fields/forest.png';
          parent.document.getElementById('$initT').src='/assets/fields/forest.png';
          parent.document.getElementById("initL").value='$serverInitL';
          parent.document.getElementById('initT').value='$serverInitT';
          parent.document.getElementById("lionH").value='$serverLionH';
          parent.document.getElementById("tigerH").value='$serverTigerH';
          parent.document.getElementById('$serverInitL').src='/assets/fields/forest_Lion.png';
          parent.document.getElementById('$serverInitT').src='/assets/fields/forest_tiger.png';
          </script>""")
  }
  fun synchronizeAfterMove(routingContext: RoutingContext, initL: String, initT: String, serverInitL: String, serverInitT: String, animation:Int) {
    routingContext.response().setStatusCode(200).putHeader("content-type", "text/html")
      .end("""
          <script>
            switch($animation) {
              case -6:
              case -2:
                parent.document.getElementById('$initL').src='/assets/fields/forest_lion.png';
                parent.document.getElementById('$initT').src='/assets/fields/forest_tiger.png';
                break;
              case -5:
                parent.document.getElementById('$initT').src='/assets/fields/forest.png';
                parent.document.getElementById('$serverInitT').src='/assets/fields/forest_tiger.png';
                break;
              case -1:
                parent.document.getElementById('$initL').src='/assets/fields/forest.png';
                parent.document.getElementById('$serverInitL').src='/assets/fields/forest_lion.png';
                break;
            }
          </script>""")
  }
  fun lionMoves(routingContext: RoutingContext, InitL: String, serverInitL: String) {
    routingContext.response().setStatusCode(200).putHeader("content-type", "text/html")
      .end("""
          <script>
          parent.document.getElementById('$InitL').src='/assets/fields/lionRun.png';
          parent.document.getElementById('$serverInitL').src='/assets/fields/forest_lion.png';
          </script>""")
  }

  fun lionAttack(routingContext: RoutingContext, serverInitL: String, serverInitT: String) {
    routingContext.response().setStatusCode(200).putHeader("content-type", "text/html")
      .end("""
          <script>
          parent.document.getElementById('$serverInitL').src='/assets/fields/lionRun.png';
          parent.document.getElementById('$serverInitT').src='/assets/fields/lionBoom.png';
          </script>""")
  }

  fun tigerMoves(routingContext: RoutingContext, InitT: String, serverInitT: String) {
    routingContext.response().setStatusCode(200).putHeader("content-type", "text/html")
      .end("""
          <script>
          parent.document.getElementById('$InitT').src='/assets/fields/tigerRun.png';
          parent.document.getElementById('$serverInitT').src='/assets/fields/forest_tiger.png';
          </script>""")
  }

  fun tigerAttack(routingContext: RoutingContext, serverInitL: String, serverInitT: String) {
    routingContext.response().setStatusCode(200).putHeader("content-type", "text/html")
      .end("""
          <script>
          parent.document.getElementById('$serverInitT').src='/assets/fields/tigerRun.png';
          parent.document.getElementById('$serverInitL').src='/assets/fields/tigerBoom.png';
          </script>""")
  }

}
