package game.webserver.router

import game.webserver.controller.DefaultController
import game.webserver.controller.GameController
import game.webserver.controller.WebController
import io.vertx.core.Vertx
import io.vertx.core.http.CookieSameSite
import io.vertx.ext.web.Router
import io.vertx.ext.web.handler.SessionHandler
import io.vertx.ext.web.handler.StaticHandler
import io.vertx.ext.web.handler.BodyHandler
import io.vertx.ext.web.sstore.LocalSessionStore
import io.vertx.ext.bridge.PermittedOptions
import io.vertx.ext.web.handler.sockjs.SockJSHandler
import io.vertx.ext.web.handler.sockjs.SockJSBridgeOptions


class GameRouter {
  fun getRouter(vertx:Vertx):Router {
    val webController = WebController()
    val defaultController = DefaultController()
    val gameController = GameController()
    val router = Router.router(vertx)
    val sessionStore = LocalSessionStore.create(vertx)
    val sessionHandler = SessionHandler.create(sessionStore)
    sessionHandler.setCookieSameSite(CookieSameSite.STRICT)
    router.route().handler(BodyHandler.create())
    router.route("/*").handler(
      StaticHandler
        .create("webroot")
        .setCachingEnabled(true)
    )
    router.get("/").handler(webController::authPage)
    router.get("/enter").handler(webController::loginPage)
    router.get("/registration").handler(webController::registerPage)
    router.get("/play").handler(webController::playPage)
    router.get("/gamePage").handler(webController::gamePage)
    //Handlers -------------------------------------------------------------
    router.post("/login").handler(webController::loginHandler)
    router.post("/register").handler(webController::registerHandler)
    router.get("/create").handler(webController::createHandler)
    router.get("/join").handler(webController::joinHandler)
    router.get("/waiting").handler(webController::joinHandler)
    //Game Handlers --------------------------------------------------------
    router.get("/online").handler(gameController::update)
    router.get("/move").handler(gameController::moveHandler)
    router.get("/finish").handler(webController::finishHandler)
    router.route().failureHandler(defaultController::handleError404)
    return router
  }
}
