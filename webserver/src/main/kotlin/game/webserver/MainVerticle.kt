package game.webserver
import io.vertx.core.AbstractVerticle
import game.webserver.router.GameRouter
import io.vertx.core.Promise

class MainVerticle:AbstractVerticle() {
  override fun start(startPromise: Promise<Void>) {
    val server = vertx.createHttpServer()
    val router = GameRouter().getRouter(vertx)
    server
      .requestHandler(router)
      .listen(8080) { request ->
        if (request.succeeded()) {
          startPromise.complete()
          println("http://localhost:8080/")
        } else {
          startPromise.fail(request.cause())
        }
      }
  }
}
