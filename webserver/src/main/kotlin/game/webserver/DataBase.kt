package game.webserver

import game.webserver.controller.GameController
import game.webserver.controller.WebController
import io.vertx.core.Vertx
import io.vertx.ext.web.RoutingContext
import io.vertx.mysqlclient.MySQLConnectOptions
import io.vertx.mysqlclient.MySQLPool
import io.vertx.sqlclient.*

class DataBase {
  fun openDataBase(): MySQLPool {
    val vertx = Vertx.currentContext().owner()
    val connectOptions = MySQLConnectOptions()
      .setPort(3306)
      .setHost("127.0.0.1")
      .setDatabase("db")
      .setUser("root")
      .setPassword("root")
    val poolOptions = PoolOptions()
      .setMaxSize(5)
    return MySQLPool.pool(vertx, connectOptions, poolOptions)
  }

  fun nameCheck(routingContext: RoutingContext, client: MySQLPool, name: String, password: String) {
    client.preparedQuery("SELECT id FROM users WHERE name=?")
      .execute(Tuple.of(name)) { query ->
        when {
          query.succeeded() -> {
            val result = query.result()
            when (result.size()) {
              0 -> register(routingContext, client, name, password)
              else -> {
                WebController().denied(routingContext, "already")
                client.close()
              }
            }
          } else -> {
            println("Failure to check names: " + query.cause().message)
            client.close()
          }
        }
      }
  }
  private fun register(routingContext: RoutingContext, client: MySQLPool, name: String, password: String) {
    client.preparedQuery("INSERT INTO users (name, password) VALUES (?,?)")
    .execute(Tuple.of(name, password)) { insert ->
      when{
        insert.succeeded() -> login(routingContext, client, name, password)
        else -> {
          println("Failure to register: " + insert.cause().message)
          client.close()
        }
      }
    }
  }

  fun login(routingContext: RoutingContext, client: MySQLPool, name: String, password: String) {
    client.preparedQuery("SELECT id FROM users WHERE name=? AND password=?")
    .execute(Tuple.of(name, password)) { login ->
      if (login.succeeded()) {
        val result = login.result()
        when (result.size()) {
          1 -> {
            for (row: Row in result) {
              val id = row.getValue("id").toString()
              WebController().access(routingContext, id, name)
            }
          } else -> WebController().denied(routingContext, "denied")
        }
        client.close()
      } else {
        println("Failure to check accounts: " + login.cause().message)
        client.close()
      }
    }
  }

  fun createRoom(routingContext: RoutingContext, client: MySQLPool, hostId: String) {
    client.preparedQuery("SELECT id FROM games WHERE host_id = ? AND player_id = ?")
    .execute(Tuple.of(hostId, 0)) { findExist ->
      if (findExist.succeeded()) {
        val result = findExist.result()
        when (result.size()) {
          0 -> {
            client.preparedQuery("INSERT INTO games (host_id,lock_id) VALUES (?,?)")
            .execute(Tuple.of(hostId, hostId)) { createNew ->
              if (createNew.succeeded()) {
                client.preparedQuery("SELECT id FROM games WHERE host_id=? AND player_id = ?")
                .execute(Tuple.of(hostId,0)) { connect ->
                  if (connect.succeeded()) {
                    val gameList = connect.result()
                    for (row: Row in gameList) {
                      val gameId = row.getValue("id").toString()
                      println("This Game id is $gameId")
                      client.close()
                      WebController().gameHandler(routingContext, hostId, gameId, "host")
                    }
                  } else {
                    println("Failure Connect to new game: " + connect.cause().message)
                    client.close()
                  }
                }
              } else {
                println("Failure to Create a new game " + createNew.cause().message)
                client.close()
              }
            }
          } else -> {
              for (row: Row in result) {
                val gameId = row.getValue("id").toString()
                println("Connect to Exist Game with id = $gameId")
                client.close()
                WebController().gameHandler(routingContext, hostId, gameId, "host")
              }
           }
        }
      } else {
        println("Failure to check created games " + findExist.cause().message)
        client.close()
      }
    }
  }

  fun joinRoom(routingContext: RoutingContext, client: MySQLPool, userId: String, gameId: String) {
    client.preparedQuery("SELECT id FROM games WHERE id = ? AND (player_id = ? OR player_id = ?)")
    .execute(Tuple.of(gameId, 0, userId)) { join ->
      if (join.succeeded()) {
        val result = join.result()
        println("Founded ${result.size()} game")
        when (result.size()) {
          1 -> {
            client.preparedQuery("UPDATE games SET player_id = ? WHERE id = ?")
            .execute(Tuple.of(userId, gameId)) { update ->
              if (update.succeeded()) {
                client.close()
                WebController().gameHandler(routingContext, userId, gameId, "player")
              } else {
                println("Failure to change player name: " + update.cause().message)
                client.close()
              }
            }
          } else -> {
          WebController().gameNotFound(routingContext, "notFounded")
          client.close()
          }
        }
      } else {
        println("Failure to join: " + join.cause().message)
        client.close()
      }
    }
  }

  fun waitingRoom(routingContext:RoutingContext, client:MySQLPool, connType:String, gameId:String, userId:String, move:Int) {
    client.preparedQuery("SELECT lock_id FROM games WHERE id = ? AND (player_id = ? OR player_id = ?)")
      .execute(Tuple.of(gameId, userId, 0, 0)) { select ->
        if (select.succeeded()) {
          val result = select.result()
          println("Founded ${result.size()} game to check Turn")
          if (result.size() == 0) {
            GameController().printFieldWrong(routingContext)
            client.close()
          }
          else logicLocker(routingContext, client, connType, gameId, move)
        } else {
          println("Failure to : " + select.cause().message)
          client.close()
        }
      }
  }

  fun checkTurn(routingContext:RoutingContext, client:MySQLPool, connType:String, gameId:String, userId:String, move:Int) {
    client.preparedQuery("SELECT lock_id, animationLion, animationTiger FROM games WHERE (id = ? AND lock_id = ? AND animationLion = ? AND animationTiger = ?)")
    .execute(Tuple.of(gameId, userId, 0, 0)) { select ->
      if (select.succeeded()) {
        val result = select.result()
        println("Founded ${result.size()} game to check Turn")
          if (result.size() == 0) {
            GameController().printFieldWrong(routingContext)
            client.close()
          }
          else logicLocker(routingContext, client, connType, gameId, move)
      } else {
        println("Failure to check turn: " + select.cause().message)
        client.close()
      }
    }
  }

  private fun logicLocker(routingContext:RoutingContext, client:MySQLPool, connType:String, gameId:String, move:Int) {
    if(connType=="host") {
      client.preparedQuery("SELECT player_id, initL, initT, lionH, tigerH FROM games WHERE id = ?")
      .execute(Tuple.of(gameId)) { select ->
        if (select.succeeded()) {
          val result = select.result()
          for (row: Row in result) {
            val newLockId = row.getValue("player_id").toString()
            val initL = row.getValue("initL").toString()
            val initT = row.getValue("initT").toString()
            val lionH = row.getValue("lionH").toString()
            val tigerH = row.getValue("tigerH").toString()
            if (newLockId == "0") {
              client.close()
              GameController().printFieldWrong(routingContext)
            }
            else GameController().logicToMove(routingContext, client, connType, gameId, newLockId, initL.toInt(), initT.toInt(), move, lionH.toInt(), tigerH.toInt())
          }
        } else {
          println("Failure to check player tile: " + select.cause().message)
          client.close()
        }
      }
    }
    else {
      client.preparedQuery("SELECT host_id, initL, initT, lionH, tigerH FROM games WHERE id = ?")
      .execute(Tuple.of(gameId)) { select ->
        if (select.succeeded()) {
          val result = select.result()
          for (row: Row in result) {
            val newLockId = row.getValue("host_id").toString()
            val initL = row.getValue("initL").toString()
            val initT = row.getValue("initT").toString()
            val lionH = row.getValue("lionH").toString()
            val tigerH = row.getValue("tigerH").toString()
            GameController().logicToMove(routingContext, client, connType, gameId, newLockId, initL.toInt(), initT.toInt(), move, lionH.toInt(), tigerH.toInt())
          }
        } else {
          println("Failure to check host tile: " + select.cause().message)
          client.close()
        }
      }
    }
  }

  fun updateLockerMove(client:MySQLPool, connType:String, gameId:String, newLockId:String, move:Int, health:Int, animation:Int) {
    if (connType == "host") {
      client.preparedQuery("UPDATE games SET lock_id = ?, initL = ?, lionH = ?, animationLion = ?, animationTiger = ? WHERE id = ?")
        .execute(Tuple.of(newLockId, move, health, animation, animation, gameId)) { update ->
          if (update.succeeded()) client.close()
          else {
            println("Failure to update HOST MOVE locker: " + update.cause().message)
            client.close()
          }
        }
    } else {
      client.preparedQuery("UPDATE games SET lock_id = ?, initT = ?, tigerH = ?, animationLion = ?, animationTiger = ? WHERE id = ?")
        .execute(Tuple.of(newLockId, move, health, animation, animation, gameId)) { update ->
          if (update.succeeded()) client.close()
            else {
            println("Failure to update PLAYER MOVE locker: " + update.cause().message)
            client.close()
          }
        }
    }
  }
  fun updateLockerAttack(client:MySQLPool, connType:String, gameId:String, newLockId:String, health:Int, animation:Int) {
    if (connType == "host") {
      client.preparedQuery("UPDATE games SET lock_id = ?, tigerH = ?, animationLion = ?, animationTiger = ? WHERE id = ?")
        .execute(Tuple.of(newLockId, health, animation, animation, gameId)) { update ->
          if (update.succeeded()) client.close()
          else {
            println("Failure to update player: " + update.cause().message)
            client.close()
          }
        }
    } else {
      client.preparedQuery("UPDATE games SET lock_id = ?, lionH = ?, animationLion = ?, animationTiger = ? WHERE id = ?")
        .execute(Tuple.of(newLockId, health, animation, animation, gameId)) { update ->
          if (update.succeeded()) client.close()
          else {
            println("Failure to update host_id tile: " + update.cause().message)
            client.close()
          }
        }
    }
  }
  fun updateLockerTotem(client:MySQLPool, connType:String, gameId:String, newLockId:String, health:Int) {
    if (connType == "host") {
      client.preparedQuery("UPDATE games SET lock_id = ?, lionH = ? WHERE id = ?")
        .execute(Tuple.of(newLockId, health, gameId)) { update ->
          if (update.succeeded()) client.close()
          else {
            println("Failure to update player: " + update.cause().message)
            client.close()
          }
        }
    } else {
      client.preparedQuery("UPDATE games SET lock_id = ?, tigerH = ? WHERE id = ?")
        .execute(Tuple.of(newLockId, health, gameId)) { update ->
          if (update.succeeded()) client.close()
          else {
            println("Failure to update host_id tile: " + update.cause().message)
            client.close()
          }
        }
    }
  }

  fun checkUpdates(routingContext:RoutingContext, client:MySQLPool, initL:String, initT:String, lionH:String, tigerH:String, gameId:String, connType:String) {
    client.preparedQuery("SELECT initL, initT, lionH, tigerH, animationLion, animationTiger FROM games WHERE id = ?")
      .execute(Tuple.of(gameId)) { select ->
        if (select.succeeded()) {
          val result = select.result()
          for (row: Row in result) {
            val serverInitL = row.getValue(0).toString()
            val serverInitT = row.getValue(1).toString()
            val serverLionH = row.getValue(2).toString()
            val serverTigerH = row.getValue(3).toString()
            val animationLion = row.getValue(4).toString()
            val animationTiger = row.getValue(5).toString()
            if(connType == "host") GameController().serverUpdateDrawer(routingContext, client, gameId, connType, initL, initT, serverInitL, serverInitT, lionH, tigerH, serverLionH, serverTigerH, animationLion.toInt())
            else GameController().serverUpdateDrawer(routingContext, client, gameId, connType, initL, initT, serverInitL, serverInitT, lionH, tigerH, serverLionH, serverTigerH, animationTiger.toInt())
          }
        } else {
          println("Failure: " + select.cause().message)
          client.close()
        }
      }
  }
  fun updateAnimation(client:MySQLPool, connType:String, gameId:String, animation:Int) {
    if(connType == "host") {
      client.preparedQuery("UPDATE games SET animationLion = ? WHERE id = ?")
        .execute(Tuple.of(animation, gameId)) { update ->
          if (update.succeeded()) client.close()
          else {
            println("Failure to update player: " + update.cause().message)
            client.close()
          }
        }
    } else {
      client.preparedQuery("UPDATE games SET animationTiger = ? WHERE id = ?")
        .execute(Tuple.of(animation, gameId)) { update ->
          if (update.succeeded()) client.close()
          else {
            println("Failure to update player: " + update.cause().message)
            client.close()
          }
        }
    }
  }

}



